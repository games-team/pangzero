pangzero (1.4.1+git20121103-6) unstable; urgency=medium

  * Declare compliance with Debian Policy 4.6.1.
  * Remove dpkg-dev from Build-Depends.
  * Remove Sam from Uploaders because he is not active anymore.
    (Closes: #1011585)

 -- Markus Koschany <apo@debian.org>  Sat, 27 Aug 2022 14:50:21 +0200

pangzero (1.4.1+git20121103-5) unstable; urgency=medium

  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.1.
  * Update homepage field because the old website returns an error message.

 -- Markus Koschany <apo@debian.org>  Sat, 02 Jan 2021 23:33:19 +0100

pangzero (1.4.1+git20121103-4) unstable; urgency=medium

  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.2.1.
  * Use canonical VCS URI.
  * d/control: Switch from priority extra to optional.
  * Drop deprecated menu file.
  * Use https for watch file URI.

 -- Markus Koschany <apo@debian.org>  Fri, 07 Dec 2018 22:12:39 +0100

pangzero (1.4.1+git20121103-3) unstable; urgency=medium

  * Team upload.
  * Fix spelling mistake in debian/changelog.
  * Add fix-pause-crash.patch and fix the broken pause feature.
    Thanks to Andrej Mernik for the report. (Closes: #857474)

 -- Markus Koschany <apo@debian.org>  Mon, 13 Mar 2017 09:01:53 +0100

pangzero (1.4.1+git20121103-2) unstable; urgency=medium

  * Moved the package to Git.
  * Update my e-mail address.
  * Declare compliance with Debian Policy 3.9.8.
  * Use only Build-Depends field because we build arch:all.
  * debian/watch: version=4
  * Add unescaped-left-brace-in-regex.patch and fix deprecation warnings that
    will become fatal with Perl 5.26. Thanks to Niko Tyni for the report.
    (Closes: #826499)

 -- Markus Koschany <apo@debian.org>  Wed, 08 Jun 2016 06:52:34 +0200

pangzero (1.4.1+git20121103-1) unstable; urgency=medium

  [ Markus Koschany ]
  * Imported Upstream Git snapshot 1.4.1+git20121103.
    - This version would not be possible without the help and patches from
      Hans de Goede. Thank you!
    - Fixes multiple serious bugs: (Closes: #692221)
      + Windowed mode does not work.
      + Sound and music not working.
      + Use of uninitialized values in comparisons. (Closes: #414336)
      + Widescreen mode crashes pangzero. (Closes: #748525). Thanks to Josh
        Triplett for the report.
      + and multiple other crashes.
  * Declare compliance with Debian Policy 3.9.6.
  * Add dpkg-dev (>= 1.17) to Build-Depends due to the use of
    dpkg-parsechangelog -S option in debian/rules.
  * Add myself to Uploaders.
  * debian/rules: Add get-orig-source target.
  * Remove Marco Rodrigues from Uploaders because he is inactive and his e-mail
    address is invalid. (Closes: #694149)
  * Add a comment in German to pangzero.desktop.
  * wrap-and-sort -sa.
  * Add libmodule-build-perl to Build-Depends-Indep to silence build warnings
    because the Build module will be removed from the next major Perl release.
  * Update 10_nowebpage.patch for new release.
  * Update debian/copyright to copyright format 1.0.

  [ Barry deFreese ]
  * Add Keywords entry to desktop file.
  * Make VCS tags canonical.

 -- Markus Koschany <apo@gambaru.de>  Fri, 03 Oct 2014 14:34:37 +0200

pangzero (1.4-1) unstable; urgency=low

  * Team upload.

  [ Jon Dowland ]
  * Remove myself from Uploaders:

  [ Paul Wise ]
  * New forked upstream version
    - Update the watch file for it
    - Compatible with, needs new version of Perl SDL bindings (Closes: #660948)
    - Update patch for new source layout
  * Switch to dpkg-source v3
  * Wrap and sort fields in various files
  * Sync desktop file to the menu file
  * Switch to debhelper compat 9 and dh rules.tiny
  * Point at the correct version of the GPL
  * Lower priority according to override
  * Bump Standards-Version

 -- Paul Wise <pabs@debian.org>  Sun, 04 Mar 2012 14:54:37 +0800

pangzero (1.3-2) unstable; urgency=low

  [ Marco Rodrigues ]
  * Add Homepage field to debian/control.
  * Remove duplicate lines about GPL file location in copyright.

  [ Barry deFreese ]
  * Fix watch file.
  * Add .desktop file (Closes: #516561).
  * 10_nowebpage.patch - Don't show page on exit. (Closes: #481998).
  * Minor syntax fixes in debian/copyright.
  * Move autotools-dev and libsdl-perl to B-D-I.
  * Bump Standards Version to 3.8.1. (No changes needed).

  [ Ansgar Burchardt ]
  * debian/control: Change XS-Vcs-* to Vcs-*

 -- Barry deFreese <bdefreese@debian.org>  Tue, 17 Mar 2009 15:24:47 -0400

pangzero (1.3-1) unstable; urgency=low

  * New upstream release. (Closes: #444918)
  * Changed priority to optional in debian/control.
  * Add quilt do Build-Depends.

 -- Marco Rodrigues <gothicx@sapo.pt>  Mon, 01 Oct 2007 21:16:45 +0100

pangzero (1.2-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New upstream release.
  * Make clean errors won't be ignored anymore.
  * 100_rotozoom_patches.diff and 200_nowebsiteonexit.diff are not needed anymore,
    removing them.
  * Removing Build-Depends on quilt.

  [ Jon Dowland ]
  * update menu section to "Games/Action" for menu policy transition.
    Thanks Linas Žvirblis.

  [ Cyril Brulebois ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields in the control file.

 -- Jon Dowland <jon@alcopop.org>  Thu, 12 Jul 2007 12:03:54 +0100

pangzero (1.1-2) unstable; urgency=low

  [ Eddy Petrișor ]
  * Don't show website on exit if the user requested not to
    (closes: #411792) - thanks Roland Mas for the patch

 -- Eddy Petrișor <eddy.petrisor@gmail.com>  Sat,  3 Mar 2007 23:48:14 +0200

pangzero (1.1-1) unstable; urgency=low

  * New upstream release (Closes: #408307).
  * debian/control:
    + Build-depend on quilt for patch management.
  * debian/patches/100_rotozoom_calls.diff:
    + New patch to fix “uninitialized value in subroutine entry” errors
      (Closes: #407743, #408851).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 20 Feb 2007 14:20:20 +0100

pangzero (0.17-1) unstable; urgency=low

  * Initial release. Closes: #381631

 -- Miriam Ruiz <little_miry@yahoo.es>  Fri, 15 Dec 2006 11:00:05 +0100

